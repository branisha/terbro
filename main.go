package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"terbro/sensors"
)

func printHelp() {
	fmt.Printf("%s <sensor> <feature> <subfeature>\n", os.Args[0])
}

func main() {
	report, _ := sensors.ListAll()

	if len(os.Args) > 1 {
		items := os.Args[1:]

		sensorsVals := make([]string, 0)

		if (len(items) % 3) != 0 {
			fmt.Println("Invalid number of parameters")
			printHelp()
			os.Exit(1)
		}

		for ok := true; ok; ok = (len(items) > 0) {
			chip := items[0]
			feature := items[1]
			subfeature := items[2]
			items = items[3:]

			item, ok := report[chip]
			if !ok {
				sensorsVals = append(sensorsVals, "-1.0")
				continue
			}

			f, ok := item[feature]
			if !ok {
				sensorsVals = append(sensorsVals, "-1.0")
				continue
			}

			sf, ok := f[subfeature]
			if !ok {
				sensorsVals = append(sensorsVals, "-1.0")
				continue
			}

			sensorsVals = append(sensorsVals, strconv.FormatFloat(sf, 'f', 1, 64))
		}

		final := strings.Join(sensorsVals, " ")

		fmt.Println(final)

	} else {
		sensors.PrettyPrintReport(report)
	}

}
