package sensors

// #cgo CFLAGS: -g -Wall
// #cgo LDFLAGS: -lsensors
// #include <stdlib.h>
// #include <sensors/sensors.h>
// #include <sensors/error.h>
import "C"
import (
	"errors"
	"fmt"
)

var sensorsActive = false

func ProcessSensors(fs []string) ([]float64, error) {

	data := make([]float64, len(fs))

	if !sensorsActive {
		err := initSensors()
		if err != nil {
			return data, err
		}
		defer closeSensors()
	}

	return data, nil
}

func PrettyPrintReport(m map[string]map[string]map[string]float64) {
	for key, v := range m {
		fmt.Printf("%s\n", key)
		for key, v := range v {
			fmt.Printf("\t%s\n", key)
			for key, v := range v {
				fmt.Printf("\t\t%s: %f\n", key, v)
			}
		}
	}
}

func ListAll() (map[string]map[string]map[string]float64, error) {

	allItems := make(map[string]map[string]map[string]float64)

	if !sensorsActive {
		err := initSensors()
		if err != nil {
			return allItems, err
		}
		defer closeSensors()
	}

	nameBuffer := C.malloc(C.sizeof_char * 1024)
	defer C.free(nameBuffer)

	v1 := C.int(0)
	chip := C.sensors_get_detected_chips((*C.sensors_chip_name)(nil), &v1)

	for ok := true; ok; ok = chip != nil {

		ptr := (*C.char)(nameBuffer)
		bufSize := C.size_t(1024)

		C.sensors_snprintf_chip_name(ptr, bufSize, chip)
		featureMap := make(map[string]map[string]float64)

		cnt := C.int(0)
		feature := C.sensors_get_features(chip, &cnt)

		for ok := true; ok; ok = feature != nil {
			featureLabel := C.sensors_get_label(chip, feature)
			sensorFeatureName := C.GoString(featureLabel)

			subfeatureMap := make(map[string]float64)

			cnt2 := C.int(0)
			subfeatures := C.sensors_get_all_subfeatures(chip, feature, &cnt2)
			for ok := true; ok; ok = subfeatures != nil {

				subfeatureValue := C.double(0)
				C.sensors_get_value(chip, subfeatures.number, &subfeatureValue)
				subfeatureMap[C.GoString(subfeatures.name)] = float64(subfeatureValue)

				subfeatures = C.sensors_get_all_subfeatures(chip, feature, &cnt2)
			}

			featureMap[sensorFeatureName] = subfeatureMap

			feature = C.sensors_get_features(chip, &cnt)

		}
		allItems[C.GoString(ptr)] = featureMap
		chip = C.sensors_get_detected_chips((*C.sensors_chip_name)(nil), &v1)
	}

	return allItems, nil
}

func closeSensors() {
	C.sensors_cleanup()
}

func initSensors() error {
	r := C.sensors_init(nil)
	if r != 0 {
		return errors.New(C.GoString(C.sensors_strerror(r)))
	}

	return nil
}
